#pragma once
class AsmImpl
{
public:
    static int Sum(int x, int y);
    static int Sub(int x, int y);
    static __int16 Mp(__int16 x, __int16 y);
    static int Div(int x, int y);

    static bool EQ(int x, int y);
    static bool NE(int x, int y);
    static bool LT(int x, int y);
    static bool GT(int x, int y);
    static bool GE(int x, int y);
    static bool LE(int x, int y);

    static bool NOT(int x, int y);
    static int And(int x, int y);
    static int Or(int x, int y);
    static int Xor(int x, int y);

    static int Ind(int* x, int index);

    static int ShiftLeft(int a, __int8 places);
    static int ShiftRight(int a, __int8 places);
    static int SAL(int a, __int8 places);
    static int SAR(int a, __int8 places);

    static void Iter(void (*f)(), int times);
};

