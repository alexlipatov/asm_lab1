#include <iostream>
#include "AsmSorts.h"

void debugPrint(int* xs, int n) {
    std::cout << "array: [";
    for (int i = 0;i < n;i++) {
        std::cout << xs[i] << " ";
    }
    std::cout << "]\n";
}

int* AsmSorts::BubbleSort(int* xs, int n)
{
	_asm {
		mov ecx, n
		dec ecx
		mov eax, xs
		L3 : mov edx, 0
		L2 : mov ebx, edx
		inc ebx
		mov ebx, [eax + 4 * ebx]
		cmp[eax + 4 * edx], ebx
		jle L1
		mov esi, edx
		inc esi
		mov edi, [eax + 4 * edx]
		mov[eax + 4 * esi], edi
		mov[eax + 4 * edx], ebx
		L1 : inc edx
		cmp edx, ecx
		jl L2
		dec ecx
		cmp ecx, 0
		jg L3
	}
}

int* AsmSorts::AnySort(int* xs, int n)
{
    return nullptr;
}

void TestCaseAsmSorts() {

    int n = 12;
    int* xs = new int[12];

    for (int i = 0;i < n;i++) {
        xs[i] = n - i;
    }

    debugPrint(xs, n);

    AsmSorts::BubbleSort(xs, n);

    debugPrint(xs, n);
}


