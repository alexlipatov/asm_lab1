#include <iostream>
#include <cassert>
#include "asm_lab.h"
#include "AsmImpl.h"
#include "AsmSorts.h"


void f() {
    std::cout << "hello?\n";
}

int main()
{
    // version 199711L
    // std::cout << __cplusplus;

    TestCaseAsmImpl();
    TestCaseAsmSorts();
}

void TestCaseAsmImpl()
{
    // Numberic operations
    assert(12 == AsmImpl::Sum(8, 4));
    assert(12 == AsmImpl::Sub(15, 3));
    assert(12 == AsmImpl::Mp(4, 3));
    assert(12 == AsmImpl::Div(24, 2));
    assert(-1 == AsmImpl::Div(24, 0));

    // EQ
    assert(1 == AsmImpl::EQ(11, 11));
    assert(0 == AsmImpl::EQ(21, 11));

    // GT
    assert(1 == AsmImpl::GT(21, 11));
    assert(0 == AsmImpl::GT(1, 11));
    assert(0 == AsmImpl::GT(21, 21));

    // NOT
    assert(1 == AsmImpl::NOT(1, 2));
    assert(0 == AsmImpl::NOT(21, 21));

    // AND
    assert(1 == AsmImpl::And(1, 1));
    assert(0 == AsmImpl::And(1, 0));
    assert(0 == AsmImpl::And(0, 1));
    assert(0 == AsmImpl::And(0, 0));

    // Or
    assert(1 == AsmImpl::Or(1, 1));
    assert(1 == AsmImpl::Or(0, 1));
    assert(1 == AsmImpl::Or(1, 0));
    assert(0 == AsmImpl::Or(0, 0));

    // Xor
    assert(0 == AsmImpl::Xor(1, 1));
    assert(1 == AsmImpl::Xor(1, 0));
    assert(1 == AsmImpl::Xor(0, 1));
    assert(0 == AsmImpl::Xor(0, 0));

    // Indexing
    int* x = new int[4];
    for (int i = 0;i < 4;i++) {
        x[i] = i + 10;
    }

    assert(10 == AsmImpl::Ind(x, 0));
    assert(11 == AsmImpl::Ind(x, 1));
    assert(12 == AsmImpl::Ind(x, 2));

    delete[] x;

    // ShiftLeft
    assert(24 == AsmImpl::ShiftLeft(12, 1));
    assert(192 == AsmImpl::ShiftLeft(12, 4));

    // ShiftRight
    assert(6 == AsmImpl::ShiftRight(12, 1));
    assert(3 == AsmImpl::ShiftRight(12, 2));

    // sal
    assert(24 == AsmImpl::SAL(12, 1));
    assert(192 == AsmImpl::SAL(12, 4));

    // sar
    assert(6 == AsmImpl::SAR(12, 1));
    assert(3 == AsmImpl::SAR(12, 2));

    // Iter
    // 5 times message to stdout
    AsmImpl::Iter(f, 5);
}

