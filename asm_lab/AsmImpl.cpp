#include "AsmImpl.h"


int AsmImpl::Sum(int x, int y) {
    _asm {
        mov eax, x
        add eax, y
    }
}

int AsmImpl::Sub(int x, int y) {
    _asm {
        mov eax, x
        sub eax, y
    }
}

__int16 AsmImpl::Mp(__int16 x, __int16 y) {
    _asm {
        mov ax, x
        mov cx, y
        mul cx
    }
}

int AsmImpl::Div(int x, int y) {
    _asm {
        ;Handling division error
        ;on y = 0 returns - 1
        cmp y, 00
        je L1
        mov edx, 0
        mov eax, x
        mov ecx, y
        div ecx
        jmp L2
        L1 :
        mov eax, -1
            L2 :
    }
}

bool AsmImpl::EQ(int x, int y) {
    _asm {
        mov eax, x
        cmp eax, y
        je L1
        mov eax, 0
        jmp L2
        L1 :
        mov eax, 1
            L2 :
    }
}

bool AsmImpl::NE(int x, int y) {
    _asm {
        mov eax, x
        cmp eax, y
        jz L1
        mov eax, 0
        jmp L2
        L1 :
        mov eax, 1
            L2 :
    }
}

bool AsmImpl::LT(int x, int y) {
    _asm {
        mov eax, x
        cmp eax, y
        jl L1
        mov eax, 0
        jmp L2
        L1 :
        mov eax, 1
            L2 :
    }
}

bool AsmImpl::GT(int x, int y) {
    _asm {
        mov eax, x
        cmp eax, y
        jg L1
        mov eax, 0
        jmp L2
        L1 :
        mov eax, 1
            L2 :
    }
}

bool AsmImpl::GE(int x, int y) {
    _asm {
        mov eax, x
        cmp eax, y
        jge L1
        mov eax, 0
        jmp L2
        L1 :
        mov eax, 1
            L2 :
    }
}

bool LE(int x, int y) {
    _asm {
        mov eax, x
        cmp eax, y
        jle L1
        mov eax, 0
        jmp L2
        L1 :
        mov eax, 1
            L2 :
    }
}

bool AsmImpl::NOT(int x, int y) {
    _asm {
        mov eax, x
        cmp eax, y
        jne L1
        mov eax, 0
        jmp L2
        L1 :
        mov eax, 1
            L2 :
    }
}

int AsmImpl::And(int x, int y) {
    _asm {
        mov eax, x
        and eax, y
    }
}

int AsmImpl::Or(int x, int y) {
    _asm {
        mov eax, x
        or eax, y
    }
}

int AsmImpl::Xor(int x, int y) {
    _asm {
        mov eax, x
        xor eax, y
    }
}

int AsmImpl::Ind(int* x, int index) {
    _asm {
        mov eax, index
        mov ebx, x
        mov eax, [ebx + 4 * eax]
    }
}

int AsmImpl::ShiftLeft(int a, __int8 places) {
    _asm {
        mov cl, places
        mov eax, a
        shl eax, cl
    }
}

int AsmImpl::ShiftRight(int a, __int8 places) {
    _asm {
        mov cl, places
        mov eax, a
        shr eax, cl
    }
}

int AsmImpl::SAL(int a, __int8 places) {
    _asm {
        mov cl, places
        mov eax, a
        sal eax, cl
    }
}

int AsmImpl::SAR(int a, __int8 places) {
    _asm {
        mov cl, places
        mov eax, a
        sar eax, cl
    }
}

void AsmImpl::Iter(void (*f)(), int times) {
    _asm {
        mov ebx, times
        begin :
        call f
            dec ebx
            cmp ebx, 0
            jne begin
    }
}