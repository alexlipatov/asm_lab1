#pragma once
class AsmSorts
{
public:
	static int* BubbleSort(int* xs, int n);
	static int* AnySort(int* xs, int n);
};

void TestCaseAsmSorts();